
open Higlo.Lang

let token_to_string = function
| Bcomment (s,_)
| Constant (s,_)
| Directive (s,_)
| Escape (s,_)
| Id (s,_)
| Keyword (_, (s, _))
| Lcomment (s,_)
| Numeric (s,_)
| String (s,_)
| Symbol (_, (s, _))
| Text (s, _) -> s
;;

let printer tokens =
  List.iter (fun t -> print_string (token_to_string t)) tokens
;;

let () = Higlo.Printers.register_printer "raw" printer;;