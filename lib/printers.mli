(*********************************************************************************)
(*                Higlo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2014-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU Lesser General Public           *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*                                                                               *)
(*********************************************************************************)

(** Printers for the higlo tool. *)

type printer = Lang.token list -> unit


(** This structure defines the (X)HTML classes to use
  when producing XML. *)
type classes =
  {
    bcomment : string ;
    constant : string ;
    directive : string ;
    escape : string ;
    id : string ;
    keyword : int -> string ;
    lcomment : string ;
    numeric : string ;
    string : string ;
    symbol : int -> string ;
    text : string ;
    title : int -> string ;
  }

(** Default X(HTML) classes. *)
val default_classes : classes

(** Map a token to an XML tree (just a [<span class="...">code</span>] node).
  @param classes is used to change the class names used in the generated
  node. *)
val token_to_xml : ?classes: classes -> Lang.token -> Xtmpl.Xml.tree

(** Same as {!token_to_xml} but return a {!Xtmpl.Rewrite.tree}. *)
val token_to_xml_rewrite : ?classes: classes -> Lang.token -> Xtmpl.Rewrite.tree

(** [to_xtmpl ~lang code] gets the lexer associated to the language [lang],
  uses it to retrieve a list of tokens (using the {!Lang.parse} function)
  and maps these tokens to XML nodes. See {!token_to_xml} about
  the [classes] parameter. *)
val to_xml : ?classes: classes -> lang:string -> string -> Xtmpl.Xml.tree list

(** Same as {!to_xml} but return a {!Xtmpl.Rewrite.tree}. *)
val to_xml_rewrite : ?classes: classes -> lang:string -> string -> Xtmpl.Rewrite.tree list

(** @raise Failure if the printer is not registered. *)
val get_printer : string -> printer

val register_printer : string -> printer -> unit