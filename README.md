higlo
=====

Syntax highlighter in OCaml.

More information at https://zoggy.frama.io/higlo/ .
